## Tools used

1. ReactJS
2. Sass
3. Webpack 4 configuration
4. React Hooks

---

## Run Instructions

1. navigate to project's home directory path.
1. run, npm install.
1. to Build, npm run build.
2. to run local dev server, npm run dev

---

## Insights

1. I decided to work with React hooks only because the application is simple, later on if it will be more. complicated, Redux can be added.
2. I had to host JSON on another dummy api to overcome CORS problems.
3. I was planning to show the rest of the images (which are hidden for now) on click on "show more photos" button, can be done later.
4. I decided to use Sass to create a theme and use variables and mixins, the implementation needs enhancement for the imports.

---