import React from "react";
import ReactDOM from "react-dom";
import "babel-polyfill";
import { Header } from "./components/Header/Header";
import { Details } from "./containers/Details/Details";
import logo from "./assets/images/logo.svg";
import "./index.scss";

const App = () => (
    <div className="page">
        <Header logo={logo} />
        <main className="page-content">
            <div className="wrapper">
                <Details />
            </div>

        </main>
    </div>
);
ReactDOM.render(
    <App />
    , document.getElementById("app"));