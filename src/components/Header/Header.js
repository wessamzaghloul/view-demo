import React from "react";

export const Header = ({ logo }) => {
    return (
        <header className="header page-header">
            <div className="wrapper">
                <a href="#" className="header-logo">
                    <img src={logo} alt="Logo" />
                </a>
            </div>

        </header>
    );
}