import React from "react";

const getSrcSet = (mediaItems) => mediaItems.map(item => `${item.Url} ${item.Width}w`);
const getSizes = (index) => {
    switch (index) {
        case 0: return '(min-width: 1280px) 787px, 100vw';
        case 1: return '(min-width: 1280px) 404px, 100vw';
        default: return '(min-width: 1280px) 197px, 100vw';
    }

};

export const Gallery = ({ media, title }) => {

    return (
        <section className="gallery">
            {
                media.map((mediaObj, index) => (

                    <figure key={mediaObj.Id} className={`gallery__item gallery__item--${index + 1}`}>
                        {mediaObj.MediaItems &&

                            <img
                                src={mediaObj.MediaItems[0].Url}
                                srcSet={getSrcSet(mediaObj.MediaItems)}
                                sizes={getSizes(index)}
                                className='gallery__img'
                                alt={title}
                            />
                        }
                    </figure>

                ))
            }
        </section>
    );
}