import React from 'react';
import logo from "../../assets/images/logo.svg";

export const MapMarker = (props) => {
    const { text } = props;
    return (
        <div className="marker">
            <img src={logo} />
            <label>{text}</label>
        </div>
    );
};