import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import { MapMarker } from '../MapMarker/MapMarker';

const MAPS_KEY = 'AIzaSyA_EfgqO7gmYmEda36_2Up1eGkQr6lMMww';


export class Map extends Component {

    render() {
        const { center, zoom, text } = this.props;
        return (
            <div style={{ height: '500px', width: '100%' }}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: MAPS_KEY }}
                    defaultCenter={center}
                    defaultZoom={zoom}
                >
                    <MapMarker
                        lat={center.lat}
                        lng={center.lng}
                        text={text}
                    />
                </GoogleMapReact>
            </div>
        );

    }
}