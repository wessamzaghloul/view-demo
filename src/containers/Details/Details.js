import React, { useState, useEffect } from 'react';
import fetch from 'cross-fetch';
import { Loader } from '../../components/Loader/Loader';
import { Gallery } from '../../components/Gallery/Gallery';
import { Map } from '../../components/Map/Map';


export const Details = () => {
    const [detailsData, setDetailsData] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        fetch(
            // serving json from fake api to overcome CORS
            'https://api.myjson.com/bins/f2084',
            {
                method: 'GET',
            }
        )

            .then(res => res.json())
            .then(response => {
                console.log(response);
                setDetailsData(response);
                setIsLoading(false);
            })
            .catch(error => console.log(error));
    }, []);

    const media = detailsData.Media && detailsData.Media.filter(m => m.Categorie === 1);

    return (
        <>

            {isLoading && <Loader />}
            {detailsData && (
                <section className='info'>
                    <h1 className='title'>{detailsData.Adres} - {detailsData.Aanvaarding}</h1>
                    <h2 className='subtitle'>{`${detailsData.Prijs && detailsData.Prijs.Koopprijs} €`}</h2>
                </section>
            )}

            {media && media.length && (
                <Gallery media={media} title={detailsData.Adres}></Gallery>
            )}

            {detailsData && (
                <section className='map-container'>
                    {/* TODO: lng/lat is not in API so I had to check a way to extract them from address but */}
                    <Map center={{ 'lat': 49.955413, 'lng': 30.337844 }} zoom={11} text={detailsData.Adres}></Map>
                </section>
            )
            }
        </>
    );
}